﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Bytescout.Spreadsheet;
using Data.Entities;
using Data.UnitOfWork;
using Excel = Microsoft.Office.Interop.Excel;

namespace BussinessLogic.Services

{
    public class UserService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly string _filePath;
        private readonly string _savePath;
        public UserService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            
            _filePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())
                .FullName,"Template\\example.xlsx");
            _savePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory())
                .FullName, "Results\\");
        }

        public string ExcelWork(string socialNumber)
        {
            User user = GetUser(socialNumber);
            if (user != null)
            {
                if (File.Exists($"{_savePath}{user.Name}.xlsx"))
                {
                    File.Delete($"{_savePath}{user.Name}.xlsx");
                }

                if (File.Exists(_filePath))
                {
                    //Открываем приложение
                    Spreadsheet document = new Spreadsheet();
                    document.LoadFromFile(_filePath);

                    //Открываем книгу, выбираем лист
                    Worksheet worksheet = document.Workbook.Worksheets.ByName("Лист1");

                    //Вставка значений в первую табличку
                    worksheet.Cell("B3").Value = user.Id;
                    worksheet.Cell("B4").Value = user.Name;
                    worksheet.Cell("B5").Value = user.BirthDate;
                    worksheet.Cell("B6").Value = user.PhoneNumber;
                    worksheet.Cell("B7").Value = user.Address;
                    worksheet.Cell("B8").Value = user.SocialNumber;

                    //Вставка значений во вторую табличку
                    worksheet.Cell("D4").Value = user.Id;
                    worksheet.Cell("E4").Value = user.Name;
                    worksheet.Cell("F4").Value = user.BirthDate;
                    worksheet.Cell("G4").Value = user.PhoneNumber;
                    worksheet.Cell("H4").Value = user.Address;
                    worksheet.Cell("I4").Value = user.SocialNumber;
                    document.SaveAs($"{_savePath}{user.Name}.xlsx");
                    if (Directory.Exists(_savePath))
                    {
                        document.SaveAs($"{_savePath}{user.Name}.xlsx");
                    }
                    else
                    {
                        Directory.CreateDirectory(_savePath);
                        document.SaveAs($"{_savePath}{user.Name}.xlsx");
                    }
                    document.Close();

                    KillProcess();
                    return "Файл сохранен";
                }
                
            }
            else
            {
                return "Пользователь не найден";
            }

            return "Ошибка сервера";

        }
        

        public User GetUser(string socialNumber)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
               var user = unitOfWork.UserRepository.GetAllAsync().Result.FirstOrDefault(c => c.SocialNumber == socialNumber);
               return user;
            }
        }

        public void KillProcess()
        {
            Process[] etc = Process.GetProcesses();
            foreach (Process anti in etc)
                if (anti.ProcessName.ToLower().Contains("excel")) anti.Kill();
        }
    }
}
