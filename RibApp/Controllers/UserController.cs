﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace RibApp.Controllers
{
    public class UserController : Controller
    {
        private readonly UserService _service;

        public UserController(UserService service)
        {
            _service = service;
        }
        [HttpGet]
        [Route("/api/save/{socialNumber}")]
        public IActionResult SaveInExcel(string socialNumber)
        {
            try
            {
                return Json(new {Result = _service.ExcelWork(socialNumber)});
            }
            catch (Exception ex)
            {
                return StatusCode(400, ex.Message);
            }
            
        }
    }
}