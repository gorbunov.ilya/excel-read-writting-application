﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.UnitOfWork;

namespace Data.DataInitializer
{
     public static class UserDataInitializer
    {
        public static void SeedData(IUnitOfWorkFactory unitOfWorkFactory)
        {
            SeedUsers(unitOfWorkFactory);
        }

        public static void SeedUsers(IUnitOfWorkFactory unitOfWorkFactory)
        {
            Dictionary<string, string> user1 = new Dictionary<string, string>()
            {
                {"Name", "Тестовый клиент1" },
                {"BirthDate", "1991-03-08" },
                {"PhoneNumber", "123" },
                {"Address", "г. Баткен" },
                {"SocialNumber", "12345678901234" }
            };

            Dictionary<string, string> user2 = new Dictionary<string, string>()
            {
                {"Name", "Тестовый клиент2" },
                {"BirthDate", "1996-04-20" },
                {"PhoneNumber", "456" },
                {"Address", "г. Бишкек" },
                {"SocialNumber", "98765432101234" }
            };
            Dictionary<string, string> user3 = new Dictionary<string, string>()
                {
                    {"Name", "Тестовый клиент3" },
                    {"BirthDate", "1995-08-04 " },
                    {"PhoneNumber", "789" },
                    {"Address", "г. Нарын" },
                    {"SocialNumber", "12345543211234" }
                };
            Dictionary<string, string> user4 = new Dictionary<string, string>()
                {
                    {"Name", "Тестовый клиент4" },
                    {"BirthDate", "1989-02-25" },
                    {"PhoneNumber", "012" },
                    {"Address", "с. Комсомольское" },
                    {"SocialNumber", "12345671234567" }
                };

            List<Dictionary<string, string>> users = new List<Dictionary<string, string>>
            {
                user1,
                user2,
                user3,
                user4
            };

            AddSeedUsers(users, unitOfWorkFactory);

        }

        private static void AddSeedUsers(List<Dictionary<string, string>> users, IUnitOfWorkFactory unitOfWorkFactory)
        {
            using (var unitOfWork = unitOfWorkFactory.MakeUnitOfWork())
            {
                foreach (var item in users)
                {
                    if (unitOfWork.UserRepository.GetAllAsync().Result
                            .FirstOrDefault(c => c.Name == item["Name"]) == null)
                    {
                        User user = new User();
                        user.Name = item["Name"];
                        user.BirthDate = Convert.ToDateTime(item["BirthDate"]);
                        user.Address = item["Address"];
                        user.SocialNumber = item["SocialNumber"];
                        user.PhoneNumber = item["PhoneNumber"];

                        unitOfWork.UserRepository.CreateAsync(user).Wait();
                        unitOfWork.CompleteAsync().Wait();
                    }
                }
            }
        }

    }
}
