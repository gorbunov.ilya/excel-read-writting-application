﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;
using Data.Repositories.User;

namespace Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

    }
}
