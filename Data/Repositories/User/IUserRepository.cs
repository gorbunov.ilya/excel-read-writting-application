﻿namespace Data.Repositories.User
{
    public interface IUserRepository : IRepository<Entities.User>
    {
    }
}