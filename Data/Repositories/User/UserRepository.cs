﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Context;

namespace Data.Repositories.User
{
    public class UserRepository : Repository<Entities.User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

    }
}
